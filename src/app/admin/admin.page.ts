import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LoadingController } from '@ionic/angular';

declare var google;

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {

    mapRef = null;

  constructor(private geolocation: Geolocation, private loadingCtrl: LoadingController) {
  }
  ngOnInit() {
    this.loadMap();
  }
    
    async loadMap()
    {
      const loading = await this.loadingCtrl.create();
      loading.present();
      const myLatLng = await this.getLocation();
      //console.log(myLatLng);
      const mapEle: HTMLElement = document.getElementById('map');
      this.mapRef = new google.maps.Map(mapEle,{
        center: myLatLng,
        zoom: 12
      });
      google.maps.event.addListenerOnce(this.mapRef, 'idle',()=>{
        //informa cuando esta cargada
        //console.log('added');
        loading.dismiss();
        this.addMarker(myLatLng.lat, myLatLng.lng);
        
      });
       
    }

 private addMarker(lat: number, lng: number)
  {
    const marker = new google.maps.Marker({
      position: {
        lat,
        lng
      },
      zoom: 8,
      map: this.mapRef,
      title: 'Hello World!'
    });
  }

  private async getLocation()
  {
      const rta = await this.geolocation.getCurrentPosition();
        return {
        lat: rta.coords.latitude,
        lng: rta.coords.longitude
      };
  }

}
