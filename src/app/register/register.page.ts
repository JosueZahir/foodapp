import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { async } from 'q';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(private authSvc:AuthService, private router: Router) { }

  ngOnInit() {
  }

 async onRegister(email, password)
  {
    /*console.log('Email', email);
    console.log('Password', password);*/
    try
    {
      const user = await this.authSvc.register(email.value, password.value);
      if(user)
      {
        const isVerified  = this.authSvc.isEmailVerified(user);
        this.redirectUser(isVerified);
        console.log('User->', user);
        //Todo: CheckEmail
      }
    }catch(error){
      console.log('Error:',error);
    }
  }

  private redirectUser(isVerified: boolean): void{
    if(isVerified){
      this.router.navigate(['admin']);
    }else{
      this.router.navigate(['verify-e']);
    }
  }

}
