import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VerifyEPage } from './verify-e.page';

const routes: Routes = [
  {
    path: '',
    component: VerifyEPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VerifyEPageRoutingModule {}
