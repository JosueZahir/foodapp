import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VerifyEPageRoutingModule } from './verify-e-routing.module';

import { VerifyEPage } from './verify-e.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VerifyEPageRoutingModule
  ],
  declarations: [VerifyEPage]
})
export class VerifyEPageModule {}
