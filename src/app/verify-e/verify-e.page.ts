import {User} from './../shared/user.interface';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-verify-e',
  templateUrl: './verify-e.page.html',
  styleUrls: ['./verify-e.page.scss'],
})
export class VerifyEPage {

  user$:Observable<User> = this.authSvc.afAuth.user;

  constructor(private authSvc: AuthService) { }

  ngOnInit() {
  }

  async onSendEmail(): Promise<void>{
    try{
      await this.authSvc.sendVerifiedEmail();
    } catch(error){
      console.log('Error->', error);
    }
    //this.authSvc.sendVerifiedEmail();
  }

  ngOnDestroy(){
    this.authSvc.logout();
  }
}
